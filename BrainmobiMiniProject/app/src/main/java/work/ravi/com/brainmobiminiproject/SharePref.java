package work.ravi.com.brainmobiminiproject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ravi on 20-11-2017.
 */

public class SharePref {
    private static SharePref sharePref = new SharePref();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static final String NAME_KEY = "userName";
    private static final String PHONE_KEY = "userPhone";
    private static final String PASSWORD_KEY = "userPassword";
    private static final String DOB_KEY = "userDob";
    private static final String LOGIN_SESSION = "userSession";

    private SharePref() {}

    //The context passed into the getInstance should be application level context.
    public static SharePref getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharePref;
    }

    public void setName( String nameData) {
        editor.putString(NAME_KEY, nameData);
        editor.commit();
    }

    public String getName() {
        return sharedPreferences.getString(NAME_KEY, "");
    }

    public void setPhone( String phoneData) {
        editor.putString(PHONE_KEY, phoneData);
        editor.commit();
    }

    public String getPhone() {
        return sharedPreferences.getString(PHONE_KEY, "");
    }

    public void setPassword( String passwordData) {
        editor.putString(PHONE_KEY, passwordData);
        editor.commit();
    }

    public String getPassword() {
        return sharedPreferences.getString(PASSWORD_KEY,"");
    }

    public void setDOB( String dobData) {
        editor.putString(DOB_KEY, dobData);
        editor.commit();
    }

    public String getDOB() {
        return sharedPreferences.getString(DOB_KEY,"");
    }

    public void setLoginSession( Boolean loginData) {
        editor.putBoolean(LOGIN_SESSION, loginData);
        editor.commit();
    }

    public Boolean getLoginSesion() {
        return sharedPreferences.getBoolean(LOGIN_SESSION,false);
    }

   /* public void removePlaceObj() {
        editor.remove(PLACE_OBJ);
        editor.commit();
    }*/

    public void clearAll() {
        editor.clear();
        editor.commit();
    }

}