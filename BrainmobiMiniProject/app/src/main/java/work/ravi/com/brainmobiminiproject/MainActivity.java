package work.ravi.com.brainmobiminiproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button mButtonLogOut;
    UserSharedPreference mUserSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButtonLogOut = findViewById(R.id.button_log_out);

        mUserSharedPreference = UserSharedPreference.getInstance(getApplicationContext());

        mButtonLogOut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button_log_out: {
                mUserSharedPreference.logOut();
                Intent intentLaunchLoginActivity = new Intent(
                        MainActivity.this, LoginActivity.class);
                startActivity(intentLaunchLoginActivity);
                finish();

            }

        }

    }
}
