package work.ravi.com.brainmobiminiproject;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by Ravi on 20-11-2017.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    //matches 10-digit numbers only
    public String REGEX_PHONE = "^[0-9]{10}$";
    public String REGEX_PASSWORD = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@]).{4,8})";
    public String REGEX_NAME = "^([A-Za-z]+)(\\s[A-Za-z]+)*\\s?$";

    EditText mEditTextUserName, mEditTextUserPhone, mEditTextUserPassword;
    TextView mTextViewUserDOB;
    private int mIntYear, mIntMonth, mIntDay, mIntBirthYear;
    Button mButtonRegisterUser;
    ProgressBar mProgressBarRegister;
    android.support.v7.widget.Toolbar mToolbarRegiterActivity;
    ActionBar mActionBarSupport;
    UserSharedPreference userSharedPreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mEditTextUserName = findViewById(R.id.editText_register_name);
        mEditTextUserPhone = findViewById(R.id.editText_register_phone);
        mEditTextUserPassword = findViewById(R.id.editText_register_password);
        mTextViewUserDOB = findViewById(R.id.textView_register_dob);
        mButtonRegisterUser = findViewById(R.id.button_register_user);
        mProgressBarRegister = findViewById(R.id.progress_bar_register);
        mButtonRegisterUser.setOnClickListener(this);
        mTextViewUserDOB.setOnClickListener(this);
        mToolbarRegiterActivity = findViewById(R.id.toolBar_register_activity);
        setSupportActionBar(mToolbarRegiterActivity);
        mActionBarSupport = getSupportActionBar();
        mActionBarSupport.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textView_register_dob: {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mIntYear = c.get(Calendar.YEAR);
                mIntMonth = c.get(Calendar.MONTH);
                mIntDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                mTextViewUserDOB.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                mIntBirthYear = year;

                            }
                        }, mIntYear, mIntMonth, mIntDay);
                datePickerDialog.show();

                break;
            }

            case R.id.button_register_user: {

                //Get User data and validate them then register
                String name = mEditTextUserName.getText().toString().trim();
                String phone = mEditTextUserPhone.getText().toString().trim();
                String password = mEditTextUserPassword.getText().toString().trim();
                String dob = mTextViewUserDOB.getText().toString().trim();

                userSharedPreference = UserSharedPreference.getInstance(getApplicationContext());

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getApplicationContext(), "Enter Name!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!name.matches(REGEX_NAME)) {
                    Toast.makeText(getApplicationContext(), "Enter a valid Name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(getApplicationContext(), "Enter Phone No!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (phone.length() != 10) {
                    Toast.makeText(getApplicationContext(), "Enter a valid Phone No!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!phone.matches(REGEX_PHONE)) {
                    Toast.makeText(getApplicationContext(), "Enter valid 10 digit Phone No!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (new String(userSharedPreference.getPhone()).equals(phone)){
                    Toast.makeText(getApplicationContext(), "Your no is already registered .\n" +
                            "Please login !", Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter Password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!password.matches(REGEX_PASSWORD)) {
                    Toast.makeText(getApplicationContext(), "Password must be 4-8 and can contain [a-z,A-z,0-9,@]", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (password.length() <= 4 || password.length() >= 8) {
                    Toast.makeText(getApplicationContext(), "Password must be betwwen 4-8 characters", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(dob)) {
                    Toast.makeText(getApplicationContext(), "DOB is Mandatory! Please Enter your DOB", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!(mIntYear - mIntBirthYear >= 18)) {
                    Toast.makeText(getApplicationContext(), "Your age must be 18+ to register", Toast.LENGTH_SHORT).show();
                    return;
                }

                mProgressBarRegister.setVisibility(View.VISIBLE);
                userSharedPreference.setName(name);
                userSharedPreference.setPhone(phone);
                userSharedPreference.setPassword(password);
                userSharedPreference.setDOB(dob);
                //userSharedPreference.setLoginSession(true);

                //Toast.makeText(this, "Welcome " + userSharedPreference.getName(), Toast.LENGTH_LONG).show();
                mProgressBarRegister.setVisibility(View.INVISIBLE);
                /*Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);*/
                Toast.makeText(getApplicationContext(), "Registered Sucessfully \n\tPlease Login", Toast.LENGTH_LONG).show();
                finish();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.intent_back_enter_anim, R.anim.intent_back_exit_anim);
        finish();
    }
}
