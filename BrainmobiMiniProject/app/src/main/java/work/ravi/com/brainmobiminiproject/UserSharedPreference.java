package work.ravi.com.brainmobiminiproject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ravi on 20-11-2017.
 */

public class UserSharedPreference {

    private static final String NAME_KEY = "UserName";
    private static final String PHONE_KEY = "UserPhone";
    private static final String PASSWORD_KEY = "UserPassword";
    private static final String DOB_KEY = "UserDob";
    private static final String LOGIN_SESSION = "UserSession";

    private static UserSharedPreference sUserSharedPreference = new UserSharedPreference();
    private static SharedPreferences sSharedPreferences;
    private static SharedPreferences.Editor sEditorSharedPreferences;


    private UserSharedPreference() {

    }

    //The context passed into the getInstance should be application level context.
    public static UserSharedPreference getInstance(Context context) {
        if (sSharedPreferences == null) {
            sSharedPreferences = context.getSharedPreferences(context.getPackageName(),
                    Activity.MODE_PRIVATE);
            sEditorSharedPreferences = sSharedPreferences.edit();
        }
        return sUserSharedPreference;
    }

    public void setName(String nameData) {
        sEditorSharedPreferences.putString(NAME_KEY, nameData);
        sEditorSharedPreferences.commit();
    }

    public String getName() {
        return sSharedPreferences.getString(NAME_KEY, "");
    }

    public void setPhone(String phoneData) {
        sEditorSharedPreferences.putString(PHONE_KEY, phoneData);
        sEditorSharedPreferences.commit();
    }

    public String getPhone() {
        return sSharedPreferences.getString(PHONE_KEY, "");
    }

    public void setPassword(String passwordData) {
        sEditorSharedPreferences.putString(PASSWORD_KEY, passwordData);
        sEditorSharedPreferences.commit();
    }

    public String getPassword() {
        return sSharedPreferences.getString(PASSWORD_KEY, "");
    }

    public void setDOB(String dobData) {
        sEditorSharedPreferences.putString(DOB_KEY, dobData);
        sEditorSharedPreferences.commit();
    }

    public String getDOB() {
        return sSharedPreferences.getString(DOB_KEY, "");
    }

    public void setLoginSession(Boolean loginData) {
        sEditorSharedPreferences.putBoolean(LOGIN_SESSION, loginData);
        sEditorSharedPreferences.commit();
    }

    public Boolean getLoginSesion() {
        return sSharedPreferences.getBoolean(LOGIN_SESSION,
                false);
    }


    public void clearAll() {
        sEditorSharedPreferences.clear();
        sEditorSharedPreferences.commit();
    }

    public void logOut() {
        sEditorSharedPreferences.putBoolean(LOGIN_SESSION, false);
        sEditorSharedPreferences.commit();
    }

}