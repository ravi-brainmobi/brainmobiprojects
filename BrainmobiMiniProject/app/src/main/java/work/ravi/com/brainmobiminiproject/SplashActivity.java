package work.ravi.com.brainmobiminiproject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ravi on 20-11-2017.
 */

public class SplashActivity extends AppCompatActivity {
    Boolean mBooleanSessionCheck;
    final int SPLASH_TIME = 1500;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        UserSharedPreference userSharedPreference =
                UserSharedPreference.getInstance(getApplicationContext());
        mBooleanSessionCheck = userSharedPreference.getLoginSesion();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mBooleanSessionCheck == true) {
                    Intent intentLaunchMainActivity = new Intent(
                            SplashActivity.this, MainActivity.class);
                    startActivity(intentLaunchMainActivity);
                    finish();
                } else {
                    Intent intentLaunchLoginActivity = new Intent(
                            SplashActivity.this, LoginActivity.class);
                    startActivity(intentLaunchLoginActivity);
                    finish();
                }

            }
        }, SPLASH_TIME);
    }
}
