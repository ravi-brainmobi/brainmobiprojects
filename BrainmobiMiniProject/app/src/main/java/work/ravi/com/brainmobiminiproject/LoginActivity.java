package work.ravi.com.brainmobiminiproject;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ravi on 20-11-2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    String mStringGetSharedPrefrencePhone, mStringGetSharedPrefrencePassword;
    EditText mEditTextPhoneNo, mEditTextPassword;
    Button mButtonLogin;
    TextView mTextViewRegister;
    UserSharedPreference mUserSharedPreference;
    ProgressBar mProgressBarLogin;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEditTextPhoneNo = findViewById(R.id.editText_login_phone);
        mEditTextPassword = findViewById(R.id.editText_login_password);
        mButtonLogin = findViewById(R.id.button_loginIn);
        mTextViewRegister = findViewById(R.id.textView_register_user);
        mProgressBarLogin = findViewById(R.id.progress_bar_login);
        mUserSharedPreference = UserSharedPreference.getInstance(getApplicationContext());
        mButtonLogin.setOnClickListener(this);
        mTextViewRegister.setOnClickListener(this);
        mStringGetSharedPrefrencePhone = mUserSharedPreference.getPhone();
        mStringGetSharedPrefrencePassword = mUserSharedPreference.getPassword();
        setSpannableString();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.button_loginIn:

                String mEditTextPhoneData = mEditTextPhoneNo.getText().toString().trim();
                String mEditTextPasswordData = mEditTextPassword.getText().toString().trim();

                if (TextUtils.isEmpty(mEditTextPhoneData)) {
                    Toast.makeText(getApplicationContext(), "Phone no is required",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(mEditTextPasswordData)) {
                    Toast.makeText(getApplicationContext(), "Enter a Password to login",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                mProgressBarLogin.setVisibility(View.VISIBLE);

                if (new String(mStringGetSharedPrefrencePhone).equals(mEditTextPhoneData)) {
                    if (new String(mStringGetSharedPrefrencePassword).equals(mEditTextPasswordData)) {
                        Intent intentLaunchMainActivity = new Intent(
                                LoginActivity.this, MainActivity.class);
                        mUserSharedPreference.setLoginSession(true);
                        startActivity(intentLaunchMainActivity);
                        mProgressBarLogin.setVisibility(View.INVISIBLE);
                        finish();
                    }else
                        Toast.makeText(getApplicationContext(), R.string.error_password_toast,Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(this, R.string.error_phone_toast,
                            Toast.LENGTH_SHORT).show();

                mProgressBarLogin.setVisibility(View.INVISIBLE);

                break;
        }
    }


    private void setSpannableString() {

        SpannableString spannableString = new SpannableString("You don't have account ? " +
                "Register");
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK
        ), 0, 24, 0);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.intent_enter_anim, R.anim.intent_exit_anim);
            }
        };
        spannableString.setSpan(clickableSpan, 25, 33, 0);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLUE
        ), 25, 33, 0);

        mTextViewRegister.setMovementMethod(LinkMovementMethod.getInstance());
        mTextViewRegister.setGravity(Gravity.CENTER);
        mTextViewRegister.setWidth(100);
        mTextViewRegister.setBackgroundColor(Color.TRANSPARENT);
        mTextViewRegister.setText(spannableString);
    }
}
