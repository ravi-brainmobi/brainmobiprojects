package work.ravi.com.fragementdatasharing;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Ravi on 19-11-2017.
 */

public class FragementTwo extends Fragment {
    EditText editText;
    String s;
    Button buttonF2;
    FragementToFragementData fragementToFragementData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_two, container, false);
        buttonF2 = v.findViewById(R.id.buttonF2);
        editText = v.findViewById(R.id.editText);
        /*s = editText.getText().toString();*/
        buttonF2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragementToFragementData.onFragmentDataSend("Counter Increment is : ");
            }
        });
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            fragementToFragementData = (FragementToFragementData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface FragementToFragementData {
        void onFragmentDataSend(String userContent);

    }
}
