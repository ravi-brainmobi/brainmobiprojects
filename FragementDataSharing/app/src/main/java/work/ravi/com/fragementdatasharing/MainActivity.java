package work.ravi.com.fragementdatasharing;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,FragementTwo.FragementToFragementData {
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Button mButtonFragementOne, mButtonFragementTwo;
    EditText editText;
    TextView textView;
    String dataForFragement;
    int counter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.edit_text);
        textView
        mButtonFragementOne = findViewById(R.id.fragement_one);
        mButtonFragementTwo = findViewById(R.id.fragement_two);
        mButtonFragementOne.setOnClickListener(this);
        mButtonFragementTwo.setOnClickListener(this);
        dataForFragement = editText.getText().toString();

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.fragement_one:  {
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                FragementOne fragementOne = new FragementOne();
                Bundle b= new Bundle();
                b.putString("Key1", "This is my Key 1");
                fragementOne.setArguments(b);
                fragmentTransaction.replace(R.id.fr1_id,fragementOne);
                fragmentTransaction.commit();

            }

                break;

            case R.id.fragement_two:  {

                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                FragementTwo fragementTwo = new FragementTwo();
                Bundle b= new Bundle();
                b.putString("Key2", "This is my Key 2");
                fragementTwo.setArguments(b);
                fragmentTransaction.replace(R.id.fr1_id,fragementTwo);
                fragmentTransaction.commit();
            }
                break;

        }
    }

    @Override
    public void onFragmentDataSend(String userContent) {
        if(userContent!=null) {
            counter++;
            fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            FragementOne fragementOne = new FragementOne();
            Bundle b= new Bundle();
            b.putString("Key1", userContent + counter);
            fragementOne.setArguments(b);
            fragmentTransaction.replace(R.id.fr1_id,fragementOne);
            fragmentTransaction.commit();

        }
    }


   /* public interface ActivityDataToFragement{
       void sendData(String data);
    }*/


}
