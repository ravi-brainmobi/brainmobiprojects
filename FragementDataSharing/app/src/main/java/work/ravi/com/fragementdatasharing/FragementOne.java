package work.ravi.com.fragementdatasharing;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Ravi on 19-11-2017.
 */

public class FragementOne extends Fragment{
    TextView textView;
    String s;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_one, container, false);
        textView = v.findViewById(R.id.text_view);
        Bundle b = getArguments();
        s = b.getString("Key1");
        textView.setText(s);
        FragementTwo fragementTwo = new FragementTwo();
        Bundle bundle = new Bundle();
        bundle.putString("Key1", s);
        fragementTwo.setArguments(bundle);

        return v;
    }

}
